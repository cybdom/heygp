package com.cybdom.heygp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cybdom.heygp.R;
import com.cybdom.heygp.classes.PatientEmrClass;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 *  A simple {@link Fragment} subclass.
 * todo
 * import calendar data from database
 * export calendar data to database
 */
public class patient_calendar extends Fragment {

    private RecyclerView rv;
    private List<PatientEmrClass> patientEmrClass;

    public patient_calendar() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_patient_calendar, container, false);
        rv = view.findViewById(R.id.recycle);
        TextView dateId = view.findViewById(R.id.dateId);
        DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy");
        String date = df.format(Calendar.getInstance().getTime());
        dateId.setText(date);
        return view;
    }
}
