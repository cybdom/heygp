package com.cybdom.heygp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cybdom.heygp.Adapters.PatientEmrAdapter;
import com.cybdom.heygp.R;
import com.cybdom.heygp.classes.PatientEmrClass;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class patient_emr extends Fragment {

    private RecyclerView rv;
    private List<PatientEmrClass> mPatientEmrClass;

    public patient_emr() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_patient_emr, container, false);
        mPatientEmrClass = new ArrayList<>();
        rv= view.findViewById(R.id.recycle);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        mPatientEmrClass.add(new PatientEmrClass("John Doe","000-00-000",
                "Doctor",
                "John Doe"));
        initializeAdapter();
        return view;
    }
    private void initializeAdapter(){
        PatientEmrAdapter adapter = new PatientEmrAdapter(mPatientEmrClass);
        rv.setAdapter(adapter);
    }
}
