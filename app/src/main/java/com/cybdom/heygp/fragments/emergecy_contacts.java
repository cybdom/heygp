package com.cybdom.heygp.fragments;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.cybdom.heygp.Adapters.emergency_contacts_adapter;
import com.cybdom.heygp.R;
import com.cybdom.heygp.classes.emergency_contacts_class;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class emergecy_contacts extends Fragment {

    static View Parentview = null;
    Dialog myDialog;
    private RecyclerView rv;
    private static List<emergency_contacts_class> mEmergencyContacts;
    public emergecy_contacts() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Parentview = inflater.inflate(R.layout.fragment_emergecy_contacts, container, false);
        mEmergencyContacts = new ArrayList<>();
        myDialog = new Dialog(getActivity());
        rv= Parentview.findViewById(R.id.recycle);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        Button addContact = Parentview.findViewById(R.id.addContact);
        addContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.setContentView(R.layout.dialog_emergency_add);
                CardView setInput = myDialog.findViewById(R.id.setInput);
                setInput.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final String full_name, tel, email,address;
                        full_name = ((EditText)myDialog.findViewById(R.id.full_name)).getText().toString();
                        tel = ((EditText)myDialog.findViewById(R.id.tel)).getText().toString();
                        email = ((EditText)myDialog.findViewById(R.id.email)).getText().toString();
                        address = ((EditText)myDialog.findViewById(R.id.address)).getText().toString();
                        mEmergencyContacts.add(new emergency_contacts_class(
                                full_name,
                                tel,
                                email,
                                address));
                        initializeAdapter();
                        myDialog.dismiss();
                    }
                });
                myDialog.show();
            }
        });
        return Parentview;
    }
    private void initializeAdapter(){
        emergency_contacts_adapter adapter = new emergency_contacts_adapter(mEmergencyContacts);
        rv.setAdapter(adapter);
    }
}
