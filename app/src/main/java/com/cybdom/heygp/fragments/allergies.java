package com.cybdom.heygp.fragments;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cybdom.heygp.Adapters.allergies_adapter;
import com.cybdom.heygp.R;
import com.cybdom.heygp.classes.allergies_class;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class allergies extends Fragment {

    private RecyclerView rv;
    private List<allergies_class> mAllergiesClass;

    public allergies() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_allergies, container, false);
        mAllergiesClass = new ArrayList<>();
        rv= view.findViewById(R.id.recycle);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAllergiesClass.add(new allergies_class("Nut Allergie"));
                initializeAdapter();
            }
        });
        return view;
    }
    private void initializeAdapter(){
        allergies_adapter adapter = new allergies_adapter(mAllergiesClass);
        rv.setAdapter(adapter);
    }

}
