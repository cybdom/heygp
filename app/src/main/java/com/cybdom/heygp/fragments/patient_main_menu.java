package com.cybdom.heygp.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cybdom.heygp.R;
import com.cybdom.heygp.classes.userSignupInfo;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class patient_main_menu extends Fragment {

    public patient_main_menu() {
        // Required empty public constructor
    }


    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    userSignupInfo mPatientProfileInfo;
    TextView userNameTextView, textViewDate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_patient_main_menu, container, false);
        userNameTextView = view.findViewById(R.id.userNameTextView);
        textViewDate = view.findViewById(R.id.textViewDate);
        // Setting date
        DateFormat df = new SimpleDateFormat("EEE d MMM yyyy");
        String date = df.format(Calendar.getInstance().getTime());
        textViewDate.setText(date);

        // Initialising Firebase
        mAuth = FirebaseAuth.getInstance();
        mDatabase =  FirebaseDatabase.getInstance().getReference().child("users")
                .child(mAuth.getUid());
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mPatientProfileInfo = dataSnapshot.getValue(userSignupInfo.class);
                userNameTextView.setText(mPatientProfileInfo.getUsername());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                userNameTextView.setText("Username");
            }
        });



        GraphView graph = view.findViewById(R.id.graph);
        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(new DataPoint[] {
                new DataPoint(5, 4500),
                new DataPoint(6, 5000),
                new DataPoint(7, 5100),
                new DataPoint(8,4000),
                new DataPoint(9, 4500),
                new DataPoint(10, 5000),
                new DataPoint(11, 5100),
                new DataPoint(12,4000)
        });
        series.setColor(Color.parseColor("#ffffff"));
        series.setDrawDataPoints(true);
        series.setThickness(2);
        series.setDataPointsRadius(5);
        series.setAnimated(true);
        graph.getGridLabelRenderer().setGridColor(Color.TRANSPARENT);
        graph.getGridLabelRenderer().setHorizontalLabelsVisible(false);
        graph.getGridLabelRenderer().setVerticalLabelsVisible(false);
        graph.addSeries(series);
        return view;
    }
    static public void startMedicineReminder(View view){
        medecine_reminder myf = new medecine_reminder();
        FragmentActivity activity = (FragmentActivity)view.getContext();
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout_container, myf);
        transaction.commit();
    }

    public static void startMedicalRecord(View view){
        patient_health_record myf = new patient_health_record();
        FragmentActivity activity = (FragmentActivity)view.getContext();
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout_container, myf);
        transaction.commit();
    }
    public static void startDietryPlan(View view){
        medications myf = new medications();
        FragmentActivity activity = (FragmentActivity)view.getContext();
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout_container, myf);
        transaction.commit();
    }
    public static void startBookDoctor(View view){
        patient_booking myf = new patient_booking();
        FragmentActivity activity = (FragmentActivity)view.getContext();
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout_container, myf);
        transaction.commit();
    }
    public static void startCalendar(View view){
        patient_calendar myf = new patient_calendar();
        FragmentActivity activity = (FragmentActivity)view.getContext();
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout_container, myf);
        transaction.commit();
    }
    public static void startPatientProfil(View view){
        patient_profile myf = new patient_profile();
        FragmentActivity activity = (FragmentActivity)view.getContext();
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout_container, myf);
        transaction.commit();
    }
    public static void startPharmacies(View view){
        pharmacies myf = new pharmacies();
        FragmentActivity activity = (FragmentActivity)view.getContext();
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout_container, myf);
        transaction.commit();
    }
    public static void startDoctorPermission(View view){
        patient_emr myf = new patient_emr();
        FragmentActivity activity = (FragmentActivity)view.getContext();
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout_container, myf);
        transaction.commit();
    }
}
