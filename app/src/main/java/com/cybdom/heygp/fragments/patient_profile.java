package com.cybdom.heygp.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cybdom.heygp.R;
import com.cybdom.heygp.classes.userSignupInfo;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class patient_profile extends Fragment {

    static View Parentview = null;
    public patient_profile() {
        // Required empty public constructor
    }
    userSignupInfo mPatientProfileInfo;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Parentview = inflater.inflate(R.layout.fragment_patient_profile, container, false);
        mAuth = FirebaseAuth.getInstance();
        mDatabase =  FirebaseDatabase.getInstance().getReference().child("users")
                .child(mAuth.getUid());
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mPatientProfileInfo = dataSnapshot.getValue(userSignupInfo.class);
                updateUI();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        return Parentview;
    }
    static public void startFamily(View view){
        // Button family CLicked -> SHow family activity
        patient_family myf = new patient_family();
        FragmentActivity activity = (FragmentActivity)Parentview.getContext();
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout_container, myf);
        transaction.commit();
    }
    static public void startSubscriptions(View view){
        // Button family CLicked -> SHow family activity
        subscriptions myf = new subscriptions();
        FragmentActivity activity = (FragmentActivity)Parentview.getContext();
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout_container, myf);
        transaction.commit();
    }
    static public void startNotes(View view){
        doctor_notes myf = new doctor_notes();
        FragmentActivity activity = (FragmentActivity)Parentview.getContext();
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout_container, myf);
        transaction.commit();
    }
    static public void startEmergency(View view){
        emergecy_contacts myf = new emergecy_contacts();
        FragmentActivity activity = (FragmentActivity)Parentview.getContext();
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout_container, myf);
        transaction.commit();
    }
    public void updateUI(){
        TextView text_username, text_uid,text_age,text_height,
                text_weight,text_bloodtype,text_heartrate,userAddressTextView,userNumberTextView;
        text_username = Parentview.findViewById(R.id.text_username);
        text_uid = Parentview.findViewById(R.id.text_uid);
        text_age = Parentview.findViewById(R.id.text_age);
        text_height = Parentview.findViewById(R.id.text_height);
        text_weight = Parentview.findViewById(R.id.text_weight);
        text_bloodtype = Parentview.findViewById(R.id.text_bloodtype);
        userAddressTextView = Parentview.findViewById(R.id.userAddressTextView);
        userNumberTextView = Parentview.findViewById(R.id.userNumberTextView);

        text_username.setText(mPatientProfileInfo.getUsername());
        text_age.setText(String.valueOf(mPatientProfileInfo.getAge()));
        text_height.setText(String.valueOf(mPatientProfileInfo.getHeight()));
        text_weight.setText(String.valueOf(mPatientProfileInfo.getWeight()));
        text_bloodtype.setText(mPatientProfileInfo.getBloodGroup());
        userAddressTextView.setText("User Address: " + mPatientProfileInfo.getUserLocation());
        userNumberTextView.setText("User Mobile: " + mPatientProfileInfo.getEmergencyContactMobile());
    }

}
