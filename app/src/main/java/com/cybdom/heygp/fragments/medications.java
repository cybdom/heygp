package com.cybdom.heygp.fragments;


import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.cybdom.heygp.Adapters.medication_adapter;
import com.cybdom.heygp.R;
import com.cybdom.heygp.classes.medication_class;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class medications extends Fragment {
    Dialog myDialog;
    private RecyclerView rv;
    private List<medication_class> mMedicationClass;


    public medications() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_medications, container, false);
        mMedicationClass = new ArrayList<>();
        myDialog = new Dialog(getActivity());
        rv= view.findViewById(R.id.recycle);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myDialog.setContentView(R.layout.dialog_medication_add);
                CardView setInput = myDialog.findViewById(R.id.setInput);
                setInput.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final String medication_name, medication_dose;
                        medication_name = ((EditText)myDialog.findViewById(R.id.medication_name)).getText().toString();
                        medication_dose = ((EditText)myDialog.findViewById(R.id.medication_dose)).getText().toString();
                        mMedicationClass.add(new medication_class(medication_name,medication_dose));
                        initializeAdapter();
                        myDialog.dismiss();
                    }
                });
                myDialog.show();
            }
        });
        return view;
    }
    private void initializeAdapter(){
        medication_adapter adapter = new medication_adapter(mMedicationClass);
        rv.setAdapter(adapter);
    }
}
