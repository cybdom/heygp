package com.cybdom.heygp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cybdom.heygp.Adapters.Doctor_notes_adapter;
import com.cybdom.heygp.R;
import com.cybdom.heygp.classes.Doctor_notes_class;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class doctor_notes extends Fragment {

    private RecyclerView rv;
    private List<Doctor_notes_class> mNotesClass;

    public doctor_notes() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_doctor_notes, container, false);
        mNotesClass = new ArrayList<>();
        rv= view.findViewById(R.id.recycle);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        mNotesClass.add(new Doctor_notes_class("Doctor who ?",
                "13/10/2018 - 07:53",
                "Algeria",
                "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\n" +
                        "tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\n" +
                        "quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\n" +
                        "consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\n" +
                        "cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\n" +
                        "proident, sunt in culpa qui officia deserunt mollit anim id est laborum."));
        initializeAdapter();
        return view;
    }
    private void initializeAdapter(){
        Doctor_notes_adapter adapter = new Doctor_notes_adapter(mNotesClass);
        rv.setAdapter(adapter);
    }

}
