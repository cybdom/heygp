package com.cybdom.heygp.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cybdom.heygp.Adapters.PatientNotificationAdapter;
import com.cybdom.heygp.R;
import com.cybdom.heygp.classes.patient_notification_Class;

import java.util.ArrayList;
import java.util.List;


public class notification extends Fragment {

    private RecyclerView rv;
    private List<patient_notification_Class> patientNotificationClass;
    public notification() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        rv = view.findViewById(R.id.recycle);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        patientNotificationClass = new ArrayList<>();
        patientNotificationClass.add( new patient_notification_Class("Booking @ 3:30 PM",
                R.drawable.ic_booking,
                Color.rgb(245,186,24)));
        patientNotificationClass.add( new patient_notification_Class("Medication Reminder",
                R.drawable.medecin_reminder,
                Color.rgb(90, 131, 241)));
        initializeAdapter();
        return view;
    }

    private void initializeAdapter(){
        PatientNotificationAdapter adapter = new PatientNotificationAdapter(patientNotificationClass);
        rv.setAdapter(adapter);
    }
}
