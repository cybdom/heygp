package com.cybdom.heygp.fragments;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.cybdom.heygp.Adapters.PatientBookingAdapter;
import com.cybdom.heygp.R;
import com.cybdom.heygp.classes.PatientBookingClass;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * todo
 * complete the booking layout
 * send data to database
 */
public class patient_booking extends Fragment {
    RelativeLayout bookings_list,booking_info;
    ScrollView first_info, payment_info;
    static View parentView;
    private RecyclerView rv;
    private List<PatientBookingClass> mPatientBookingClass;
    public patient_booking() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_patient_booking, container, false);
        parentView = view;
        mPatientBookingClass =  new ArrayList<>();
        // Defining the recycler view
        rv = view.findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);

        // Defining the main layouts
        bookings_list = view.findViewById(R.id.bookings_list);
        booking_info = view.findViewById(R.id.booking_info);

        // Defining the scroll layouts
        first_info = view.findViewById(R.id.first_info);
        payment_info = view.findViewById(R.id.payment_info);

        //        Add new booking;
        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookings_list.setVisibility(View.INVISIBLE);
                booking_info.setVisibility(View.VISIBLE);
            }
        });
        //        Listening for buttons
        TextView continueButton = view.findViewById(R.id.continueBooking),
                confirmBooking = view.findViewById(R.id.confirmBooking);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                first_info.setVisibility(View.INVISIBLE);
                payment_info.setVisibility(View.VISIBLE);
            }
        });
        confirmBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookings_list.setVisibility(View.VISIBLE);
                booking_info.setVisibility(View.INVISIBLE);
                payment_info.setVisibility(View.INVISIBLE);
                first_info.setVisibility(View.VISIBLE);
                confirmeBooking();
            }
        });
        return view;
    }
    public void confirmeBooking(){
       mPatientBookingClass.add(new PatientBookingClass("Doctor","11/12/2018"));
       initializeAdapter();
    }
    private void initializeAdapter(){
        PatientBookingAdapter adapter = new PatientBookingAdapter(mPatientBookingClass);
        rv.setAdapter(adapter);
    }
    public static void selectCallType(View view){
        CardView VideoButton, AudioButton, selectedButton = (CardView)view;
        VideoButton = parentView.findViewById(R.id.VideoButton);
        AudioButton = parentView.findViewById(R.id.AudioButton);
        if ( view == VideoButton){
            AudioButton.setAlpha((float).5);
        }else if (view == AudioButton){
            VideoButton.setAlpha((float).5);
        }
        if ( selectedButton.getAlpha() != 1 ){
            selectedButton.animate().alpha(1).setDuration(50).start();
        }else{
            selectedButton.animate().alpha((float).5).setDuration(50).start();
        }
    }
}
