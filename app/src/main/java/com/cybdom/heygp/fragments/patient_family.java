package com.cybdom.heygp.fragments;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.cybdom.heygp.Adapters.PatientFamilyAdapter;
import com.cybdom.heygp.R;
import com.cybdom.heygp.classes.PatientFamilyClass;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * todo
 * add family members to database
 * get family members from database
 */
public class patient_family extends Fragment {

    Dialog myDialog;
    private RecyclerView rv;
    private List<PatientFamilyClass> mPatientFamilyClass;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;

    public patient_family() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_patient_family, container, false);
        mPatientFamilyClass = new ArrayList<>();
//        Loading firebase;
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.
                getInstance().
                getReference().
                child("users").
                child(mAuth.getUid()).
                child("family");
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> familyMembers = dataSnapshot.getChildren();
                for (DataSnapshot member : familyMembers){
                    mPatientFamilyClass.add((member.getValue(PatientFamilyClass.class)));
                }
                initializeAdapter();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        myDialog = new Dialog(view.getContext());
        rv = view.findViewById(R.id.recycle);
        LinearLayoutManager llm = new LinearLayoutManager(view.getContext());
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        FloatingActionButton fab = view.findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myDialog.setContentView(R.layout.dialog_patient_add);
                CardView setInput = myDialog.findViewById(R.id.setInput);
                setInput.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String first_name, surname, other_names, date_birth, marial_status,
                                address, tel, email;
//                        first_name = ((EditText)myDialog.
//                                findViewById(R.id.first_name)).
//                                getText().
//                                toString();
//                        surname = ((EditText)myDialog.
//                                findViewById(R.id.surname)).
//                                getText().
//                                toString();
//                        other_names = ((EditText)myDialog.
//                                findViewById(R.id.other_names)).
//                                getText().
//                                toString();
//                        date_birth = ((EditText)myDialog.
//                                findViewById(R.id.date_birth)).
//                                getText().
//                                toString();
//                        marial_status = ((Spinner)myDialog.
//                                findViewById(R.id.marial_status)).
//                                getSelectedItem().
//                                toString();
//                        address = ((EditText)myDialog.
//                                findViewById(R.id.address)).
//                                getText().
//                                toString();
//                        tel = ((EditText)myDialog.
//                                findViewById(R.id.tel)).
//                                getText().
//                                toString();
//                        email = ((EditText)myDialog.
//                                findViewById(R.id.email)).
//                                getText().
//                                toString();
//                        mPatientFamilyClass.
//                                add(new PatientFamilyClass(
//                                first_name + " " + surname,
//                                other_names,
//                                date_birth,
//                                address,
//                                marial_status,
//                                tel,
//                                email
//                        ));
//                        mDatabase.setValue(mPatientFamilyClass);
                        initializeAdapter();
                        myDialog.dismiss();
                    }
                });
                myDialog.show();
            }
        });
        return view;
    }
    public void toogleExpand(View view){
        Boolean isVisible = false;
        View expandView = (View)view.
                getParent().
                getParent();
        LinearLayout expand = expandView.
                findViewById(R.id.expanded);
        Log.i("Visibility", String.
                valueOf(expand.getVisibility()));

        if ( expand.getVisibility() == View.VISIBLE) {
            isVisible = true;
        }
        if ( expand.getVisibility() == View.GONE) {
            isVisible = false;
        }
        if (isVisible){expand.setVisibility(View.GONE);view.animate().rotation(0);}
        else{expand.setVisibility(View.VISIBLE);view.animate().rotation(180);}
    }

    private void initializeAdapter(){
        PatientFamilyAdapter adapter = new PatientFamilyAdapter(mPatientFamilyClass);
        rv.setAdapter(adapter);
    }
}
