package com.cybdom.heygp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cybdom.heygp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class audio_call extends Fragment {


    public audio_call() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_audio_call, container, false);
        return view;
    }

}
