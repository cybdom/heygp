package com.cybdom.heygp.fragments;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cybdom.heygp.Adapters.pharmacie_adapter;
import com.cybdom.heygp.R;
import com.cybdom.heygp.classes.pharmacie_class;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class pharmacies extends Fragment implements OnMapReadyCallback {
    private RecyclerView rv;
    private List<pharmacie_class> mPharmacieClass;
    private GoogleMap gmap;
    private MapView mapView;
    private static final String MAP_VIEW_BUNDLE_KEY = "AIzaSyCFXc65-lSvVyW2SbbwhnpzVrvZYmXcCmk  ";
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;

    public pharmacies() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pharmacies, container, false);
        mPharmacieClass = new ArrayList<>();
        mAuth = FirebaseAuth.getInstance();
        mDatabase =  FirebaseDatabase.getInstance().getReference().child("pharmacies");

        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }
        mapView = view.findViewById(R.id.mapView);
        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(this);

        rv= view.findViewById(R.id.recycle);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        initializeAdapter();
        return view;
    }
    private void initializeAdapter(){
        pharmacie_adapter adapter = new pharmacie_adapter(mPharmacieClass);
        rv.setAdapter(adapter);
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }

        mapView.onSaveInstanceState(mapViewBundle);
    }
    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }
    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }
    @Override
    public void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
    @Override
    public void onMapReady(final GoogleMap googleMap) {
        gmap = googleMap;
        gmap.setMinZoomPreference(10);
        LatLng initial = new LatLng(10.424086, -61.264100);
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(initial));
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> pharmacie = dataSnapshot.getChildren();
                for (DataSnapshot member : pharmacie){
                    mPharmacieClass.add((member.getValue(pharmacie_class.class)));
                    if (!mPharmacieClass.isEmpty()) {
                        for (int a = 0; a < mPharmacieClass.size(); a++) {
                            googleMap.addMarker(new MarkerOptions().position(new LatLng(mPharmacieClass.get(a).getLatitude(),
                                    mPharmacieClass.get(a).getLongitude()))
                                    .title(mPharmacieClass.get(a).getPharmacy_name()));
                            Log.i("Position",String.valueOf( mPharmacieClass.get(a).getLatitude()) + " " + String.valueOf( mPharmacieClass.get(a).getLongitude()));
                        }
                        initializeAdapter();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
    public static void openDialog(View v){
        Dialog myDialog;
        myDialog = new Dialog(v.getContext());
        myDialog.setContentView(R.layout.dialog_pharmacie);
        myDialog.show();
    }
}
