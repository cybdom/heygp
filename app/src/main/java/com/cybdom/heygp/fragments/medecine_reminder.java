package com.cybdom.heygp.fragments;


import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.cybdom.heygp.Adapters.Medecine_reminder_adapter;
import com.cybdom.heygp.R;
import com.cybdom.heygp.classes.reminderClass;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */

/**
 * todo
 * 1- Send content to database
 * 2- Save added medecins locally
 * 3- change the way we set the medecine time
 */
public class medecine_reminder extends Fragment {
    Dialog myDialog;
    private RecyclerView rv;
    private List<reminderClass> mReminderClass;

    public medecine_reminder() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_medecine_reminder, container, false);
        mReminderClass = new ArrayList<>();
        myDialog = new Dialog(getActivity());
        rv= view.findViewById(R.id.recycle);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myDialog.setContentView(R.layout.dialog_reminder_add);
                CardView setInput = myDialog.findViewById(R.id.setInput);
                setInput.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final String medication_string, schedule_string, notes_string;
                        medication_string = ((EditText)myDialog.findViewById(R.id.medication_string)).getText().toString();
                        schedule_string = ((EditText)myDialog.findViewById(R.id.schedule_string)).getText().toString();
                        notes_string = ((EditText)myDialog.findViewById(R.id.notes_string)).getText().toString();
                        mReminderClass.add(new reminderClass(medication_string,schedule_string,notes_string));
                        initializeAdapter();
                        myDialog.dismiss();
                    }
                });
                myDialog.show();
            }
        });
        return view;
    }

    private void initializeAdapter(){
        Medecine_reminder_adapter adapter = new Medecine_reminder_adapter(mReminderClass);
        rv.setAdapter(adapter);
    }

}
