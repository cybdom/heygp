package com.cybdom.heygp;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cybdom.heygp.classes.userSignupInfo;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class activity_signup extends AppCompatActivity {

    int currentLayout = 0, bloodButtonId = 0, REQUEST_CODE = 123;
    userSignupInfo mUserSignupInfo = new userSignupInfo();
    private DatabaseReference mDatabase;
    List<LinearLayout> layoutsList;
    ArrayList<LinearLayout> mLinearLayoutArray;
    String LocationProvider = LocationManager.GPS_PROVIDER, bloodName ="", userPassword;
    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mLinearLayoutArray = new ArrayList<>();
        mLinearLayoutArray.add((LinearLayout) findViewById(R.id.emailLayout));
        mLinearLayoutArray.add((LinearLayout) findViewById(R.id.infoLayout));
        mLinearLayoutArray.add((LinearLayout) findViewById(R.id.dateLayout));
        mLinearLayoutArray.add((LinearLayout) findViewById(R.id.bloodLayout));
        mLinearLayoutArray.add((LinearLayout) findViewById(R.id.emergencyLayout));
        mLinearLayoutArray.add((LinearLayout) findViewById(R.id.locationLayout));
    }

    public void next(View view) {
        if ( dataValidation(currentLayout) ) {
            mLinearLayoutArray.get(currentLayout).setVisibility(View.INVISIBLE);
            currentLayout++;
            if (currentLayout < mLinearLayoutArray.size()) {
                mLinearLayoutArray.get(currentLayout).setVisibility(View.VISIBLE);
            } else if (currentLayout == mLinearLayoutArray.size()) {
                sendData();
            }
        }
    }

    public void previous(View view) {
        if (currentLayout == 0) {
            finish();
        }
        mLinearLayoutArray.get(currentLayout).setVisibility(View.INVISIBLE);
        currentLayout--;
        if (currentLayout >= 0) {
            mLinearLayoutArray.get(currentLayout).setVisibility(View.VISIBLE);
        }
    }

    public void chooseDate(View view) {
        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        final int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);
        DatePickerDialog picker = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        monthOfYear = view.getMonth()+1;
                        ((TextView) findViewById(R.id.dateId)).setText(String.valueOf(dayOfMonth + " / " + monthOfYear + " / " + year));

                    }
                }, year, month, day);
        picker.show();
    }
//  Old selected bloodType
    TextView OldBloodType = null;
    public void selectedBlood(View view) {
        if (OldBloodType != null){
            OldBloodType.setTextColor(Color.parseColor("#666666"));
        }
        TextView SelectedbloodName = view.findViewById(R.id.bloodName);
        OldBloodType = SelectedbloodName;
        bloodName = SelectedbloodName.getText().toString();
        SelectedbloodName.setTextColor(Color.BLACK);
        if ( bloodName.matches("Not Sure")){
            bloodName = "?";
        }
        mUserSignupInfo.setBloodGroup(bloodName);
    }
    public void saveUserInput(int currentLayout) {
        if (currentLayout == 0) {
                mUserSignupInfo.setEmailAddress(((EditText) findViewById(R.id.emailAddress)).getText().toString());
                userPassword = ((EditText) findViewById(R.id.password)).getText().toString().trim();
        } else if (currentLayout == 1) {
            mUserSignupInfo.setFirstName(((EditText) findViewById(R.id.firstName)).getText().toString());
            mUserSignupInfo.setSecondName(((EditText) findViewById(R.id.secondName)).getText().toString());
            mUserSignupInfo.setUsername(mUserSignupInfo.getFirstName() + " " + mUserSignupInfo.getSecondName());
            if (((Spinner) findViewById(R.id.gender)).getSelectedItemPosition() == 0) {
                mUserSignupInfo.setGender("Male");
            } else if (((Spinner) findViewById(R.id.gender)).getSelectedItemPosition() == 1) {
                mUserSignupInfo.setGender("Female");
            }
        } else if (currentLayout == 2) {
            mUserSignupInfo.setDateOfBirth(((TextView) findViewById(R.id.dateId)).getText().toString());
        } else if (currentLayout == 3) {
        } else if (currentLayout == 4) {
            mUserSignupInfo.setEmergencyContactName(((EditText) findViewById(R.id.emergencyName)).getText().toString());
            mUserSignupInfo.setEmergencyContactEmail(((EditText) findViewById(R.id.emergencyEmail)).getText().toString());
            mUserSignupInfo.setEmergencyContactLandline(((EditText) findViewById(R.id.emergencyLandLine)).getText().toString());
            mUserSignupInfo.setEmergencyContactMobile(((EditText) findViewById(R.id.emergencyMobile)).getText().toString());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Log.i("Permission", "Permission Acquired");
                Toast.makeText(this, "Getting Current Location ...", Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(this, "Please accept permission or select a city", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void useCurrentLocation(View view) {
        mUserSignupInfo.setUserLocation(null);
        LocationManager locationManager;
        LocationListener locationListener;
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.i("Location", "Location Changed");
                Log.i("Location", String.valueOf(location.getLatitude()));
                Log.i("Location", String.valueOf(location.getLongitude()));
                String locationString = "Lat:"+String.valueOf(location.getLatitude()) + " Long:" + String.valueOf(location.getLongitude());
                Toast.makeText(activity_signup.this, "User Location Set", Toast.LENGTH_SHORT).show();
                mUserSignupInfo.setUserLocation(locationString);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

                Log.i("Location", "Location Disabled");
                Toast.makeText(activity_signup.this, "Error while getting location", Toast.LENGTH_SHORT).show();
            }
        };
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //  Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this,
                    new String[] {Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_CODE);
            return;
        }

        locationManager.requestLocationUpdates(LocationProvider, 5000, 100, locationListener);
    }

    public void sendData(){
        mAuth.createUserWithEmailAndPassword(mUserSignupInfo.getEmailAddress(), userPassword)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("result", "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            assert user != null;
                            Toast.makeText(activity_signup.this, "hi " + user.getEmail(), Toast.LENGTH_SHORT).show();
                            saveToDatabase(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("result", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(getApplicationContext(), task.getException().getMessage(),
                                    Toast.LENGTH_SHORT).show();
                            currentLayout = 0;
                            mLinearLayoutArray.get(0).setVisibility(View.VISIBLE);
                        }
                    }
                });
    }
    public void UpdateUi(){
        Intent i = new Intent(getApplicationContext(), patient_mainMenu.class);
        startActivity(i);
    }

    public boolean dataValidation (int currentLayout){
        if ( currentLayout == 0 ){
            String email,emailConfirmation, password;
            email = ((EditText)findViewById(R.id.emailAddress)).getText().toString().trim();
            emailConfirmation = ((EditText)findViewById(R.id.emailAddressConfirmation)).getText().toString().trim();
            password = ((EditText) findViewById(R.id.password)).getText().toString().trim();

            if ( !Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                Toast.makeText(this, "Invalid Email", Toast.LENGTH_SHORT).show();
                return false;
            }
            if (!email.matches(emailConfirmation)){
                Toast.makeText(this, "Email confirmation doesn't match email", Toast.LENGTH_SHORT).show();
                return false;
            }
            if ( password.length() < 8){
                Toast.makeText(this, "Password should be longer than 8 characters", Toast.LENGTH_SHORT).show();
                return false;
            }
            saveUserInput( 0 );
            return true;
        }
        else if (currentLayout == 1 ){
            String firstName = ((EditText) findViewById(R.id.firstName)).getText().toString().trim();
            String secondName = ((EditText) findViewById(R.id.secondName)).getText().toString().trim();
            if (firstName.length() < 3){
                Toast.makeText(this, "Invalid First Name", Toast.LENGTH_SHORT).show();
                return false;
            }
            if (secondName.length() < 3){
                Toast.makeText(this, "Invalid Second Name", Toast.LENGTH_SHORT).show();
                return false;
            }
            saveUserInput( 1 );
            return true;
        }
        else if ( currentLayout == 2 ){
            return true;
        }
        else if ( currentLayout == 3 ){
            if (!(bloodName.length() > 0 )) {
                Toast.makeText(this, "Select your blood type", Toast.LENGTH_SHORT).show();
                return false;
            }
            return true;
        }
        else if ( currentLayout == 4){
            String emergencyName, emergencyEmail, emergencyLandLine, emergencyMobile;
            emergencyName = ((EditText)findViewById(R.id.emergencyName)).getText().toString().trim();
            emergencyEmail = ((EditText)findViewById(R.id.emergencyEmail)).getText().toString().trim();
            emergencyLandLine = ((EditText)findViewById(R.id.emergencyLandLine)).getText().toString().trim();
            emergencyMobile = ((EditText)findViewById(R.id.emergencyMobile)).getText().toString().trim();

            if (emergencyName.length() < 3){
                Toast.makeText(this, "Invalid Name", Toast.LENGTH_SHORT).show();
                return false;
            }
            if (!Patterns.EMAIL_ADDRESS.matcher(emergencyEmail).matches()){
                Toast.makeText(this, "Invalid Email", Toast.LENGTH_SHORT).show();
                return false;
            }
            if (emergencyLandLine.length()>0 && !emergencyLandLine.matches("0+[0-9]{9}")){
                Toast.makeText(this, "Invalid LandLine Number", Toast.LENGTH_SHORT).show();
                return false;
            }
            if (!emergencyMobile.matches("0+[0-9]{9}")){
                Toast.makeText(this, "Invalid Mobile Number", Toast.LENGTH_SHORT).show();
                return false;
            }
            saveUserInput(4);
            return true;
        }
        else if ( currentLayout == 5 ){
            if ( mUserSignupInfo.getUserLocation() == null ){
                Toast.makeText(this, "Please Select a location", Toast.LENGTH_SHORT).show();
                return false;
            }
            return true;
        }
        return false;
    }

    public void saveToDatabase (FirebaseUser user){
        mDatabase.child("users").child(user.getUid()).setValue(mUserSignupInfo);
        UpdateUi();
    }
}
