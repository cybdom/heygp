package com.cybdom.heygp.classes;

public class medication_class {
    public String medication_name,medication_dose;
    public medication_class(){}
    public medication_class(String medication_name, String medication_dose) {
        this.medication_name = medication_name;
        this.medication_dose = medication_dose;
    }

    public String getMedication_name() {
        return medication_name;
    }

    public void setMedication_name(String medication_name) {
        this.medication_name = medication_name;
    }

    public String getMedication_dose() {
        return medication_dose;
    }

    public void setMedication_dose(String medication_dose) {
        this.medication_dose = medication_dose;
    }
}
