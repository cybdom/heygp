package com.cybdom.heygp.classes;

public class userSignupInfo {
    String emailAddress, mobileNumber, username, firstName, secondName, gender, dateOfBirth, bloodGroup,
            emergencyContactName, emergencyContactEmail, emergencyContactLandline,
            emergencyContactMobile, userLocation ;
    Long age, height, weight;
    public userSignupInfo(){}

    public userSignupInfo(String emailAddress, String mobileNumber, String username, String firstName,
                          String secondName, String gender, String dateOfBirth, String bloodGroup,
                          String emergencyContactName, String emergencyContactEmail,
                          String emergencyContactLandline, String emergencyContactMobile,
                          String userLocation,
                          Long age, Long height, Long weight) {
        this.emailAddress = emailAddress;
        this.mobileNumber = mobileNumber;
        this.username = username;
        this.firstName = firstName;
        this.secondName = secondName;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.bloodGroup = bloodGroup;
        this.emergencyContactName = emergencyContactName;
        this.emergencyContactEmail = emergencyContactEmail;
        this.emergencyContactLandline = emergencyContactLandline;
        this.emergencyContactMobile = emergencyContactMobile;
        this.userLocation = userLocation;
        this.age = age;
        this.height = height;
        this.weight = weight;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getEmergencyContactName() {
        return emergencyContactName;
    }

    public void setEmergencyContactName(String emergencyContactName) {
        this.emergencyContactName = emergencyContactName;
    }

    public String getEmergencyContactEmail() {
        return emergencyContactEmail;
    }

    public void setEmergencyContactEmail(String emergencyContactEmail) {
        this.emergencyContactEmail = emergencyContactEmail;
    }

    public String getEmergencyContactLandline() {
        return emergencyContactLandline;
    }

    public void setEmergencyContactLandline(String emergencyContactLandline) {
        this.emergencyContactLandline = emergencyContactLandline;
    }

    public String getEmergencyContactMobile() {
        return emergencyContactMobile;
    }

    public void setEmergencyContactMobile(String emergencyContactMobile) {
        this.emergencyContactMobile = emergencyContactMobile;
    }

    public String getUserLocation() {
        return userLocation;
    }

    public void setUserLocation(String userLocation) {
        this.userLocation = userLocation;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public Long getHeight() {
        return height;
    }

    public void setHeight(Long height) {
        this.height = height;
    }

    public Long getWeight() {
        return weight;
    }

    public void setWeight(Long weight) {
        this.weight = weight;
    }
}
