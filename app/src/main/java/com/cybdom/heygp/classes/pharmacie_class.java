package com.cybdom.heygp.classes;

public class pharmacie_class {
    public String pharmacy_name, pharmacie_address, pharmacie_hours;
    double latitude, longitude;
    public pharmacie_class() {
    }

    public pharmacie_class(String pharmacy_name, String pharmacie_address, String pharmacie_hours, double latitude, double longitude) {
        this.pharmacy_name = pharmacy_name;
        this.pharmacie_address = pharmacie_address;
        this.pharmacie_hours = pharmacie_hours;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getPharmacy_name() {
        return pharmacy_name;
    }

    public void setPharmacy_name(String pharmacy_name) {
        this.pharmacy_name = pharmacy_name;
    }

    public String getPharmacie_address() {
        return pharmacie_address;
    }

    public void setPharmacie_address(String pharmacie_address) {
        this.pharmacie_address = pharmacie_address;
    }

    public String getPharmacie_hours() {
        return pharmacie_hours;
    }

    public void setPharmacie_hours(String pharmacie_hours) {
        this.pharmacie_hours = pharmacie_hours;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}