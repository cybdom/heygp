package com.cybdom.heygp.classes;

public class patient_notification_Class {
    public String notification_text;
    public int notification_icon;
    public int card_color;

    public patient_notification_Class(){}

    public patient_notification_Class(String notification_text, int notification_icon, int card_color) {
        this.notification_text = notification_text;
        this.notification_icon = notification_icon;
        this.card_color = card_color;
    }

    public String getNotification_text() {
        return notification_text;
    }

    public void setNotification_text(String notification_text) {
        this.notification_text = notification_text;
    }

    public int getNotification_icon() {
        return notification_icon;
    }

    public void setNotification_icon(int notification_icon) {
        this.notification_icon = notification_icon;
    }

    public int getCard_color() {
        return card_color;
    }

    public void setCard_color(int card_color) {
        this.card_color = card_color;
    }
}
