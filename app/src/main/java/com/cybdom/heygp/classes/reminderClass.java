package com.cybdom.heygp.classes;

public class reminderClass {
    public String medication_string;
    public String schedule_string;
    public String notes_string;
    public reminderClass(){}
    public reminderClass(String medication_string, String schedule_string, String notes_string) {
        this.medication_string = medication_string;
        this.schedule_string = schedule_string;
        this.notes_string = notes_string;
    }

    public String getMedication_string() {
        return medication_string;
    }

    public void setMedication_string(String medication_string) {
        this.medication_string = medication_string;
    }

    public String getSchedule_string() {
        return schedule_string;
    }

    public void setSchedule_string(String schedule_string) {
        this.schedule_string = schedule_string;
    }

    public String getNotes_string() {
        return notes_string;
    }

    public void setNotes_string(String notes_string) {
        this.notes_string = notes_string;
    }
}
