package com.cybdom.heygp.classes;

public class PatientFamilyClass {
    public String fullNameString;
    public String other_names;
    public String ageString;
    public String addressString;
    public String marialStatusString;
    public String phoneNumberString;
    public String emailString;
    public PatientFamilyClass(){}
    public PatientFamilyClass(String fullNameString, String other_names, String ageString, String addressString, String marialStatusString, String phoneNumberString, String emailString) {
        this.fullNameString = fullNameString;
        this.other_names = other_names;
        this.ageString = ageString;
        this.addressString = addressString;
        this.marialStatusString = marialStatusString;
        this.phoneNumberString = phoneNumberString;
        this.emailString = emailString;
    }

    public String getFullNameString() {
        return fullNameString;
    }

    public void setFullNameString(String fullNameString) {
        this.fullNameString = fullNameString;
    }

    public String getOther_names() {
        return other_names;
    }

    public void setOther_names(String other_names) {
        this.other_names = other_names;
    }

    public String getAgeString() {
        return ageString;
    }

    public void setAgeString(String ageString) {
        this.ageString = ageString;
    }

    public String getAddressString() {
        return addressString;
    }

    public void setAddressString(String addressString) {
        this.addressString = addressString;
    }

    public String getMarialStatusString() {
        return marialStatusString;
    }

    public void setMarialStatusString(String marialStatusString) {
        this.marialStatusString = marialStatusString;
    }

    public String getPhoneNumberString() {
        return phoneNumberString;
    }

    public void setPhoneNumberString(String phoneNumberString) {
        this.phoneNumberString = phoneNumberString;
    }

    public String getEmailString() {
        return emailString;
    }

    public void setEmailString(String emailString) {
        this.emailString = emailString;
    }
}
