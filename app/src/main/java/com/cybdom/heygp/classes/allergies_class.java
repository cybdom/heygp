package com.cybdom.heygp.classes;

public class allergies_class {
    public String allergies;

    public allergies_class(String allergies) {
        this.allergies = allergies;
    }

    public String getAllergies() {
        return allergies;
    }

    public void setAllergies(String allergies) {
        this.allergies = allergies;
    }
}
