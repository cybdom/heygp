package com.cybdom.heygp.classes;

public class emergency_contacts_class {
    public String full_name, tel, address, email;
    public emergency_contacts_class(){}
    public emergency_contacts_class(String full_name, String tel, String address, String email) {
        this.full_name = full_name;
        this.tel = tel;
        this.address = address;
        this.email = email;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
