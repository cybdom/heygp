package com.cybdom.heygp.classes;

public class PatientBookingClass {
    String Profession, Date;
    public PatientBookingClass(){}
    public PatientBookingClass(String profession, String date) {
        Profession = profession;
        Date = date;
    }

    public String getProfession() {
        return Profession;
    }

    public void setProfession(String profession) {
        Profession = profession;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }
}
