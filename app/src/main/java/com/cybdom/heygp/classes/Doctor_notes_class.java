package com.cybdom.heygp.classes;

public class Doctor_notes_class {
    public String name_surname, date, location, note;
    public Doctor_notes_class(){}
    public Doctor_notes_class(String name_surname, String date, String location, String note) {
        this.name_surname = name_surname;
        this.date = date;
        this.location = location;
        this.note = note;
    }

    public String getName_surname() {
        return name_surname;
    }

    public void setName_surname(String name_surname) {
        this.name_surname = name_surname;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
