package com.cybdom.heygp.classes;

public class PatientEmrClass {
    public String doctor_name;
    public String emr_id;
    public String profession;
    public String hospital;
    public PatientEmrClass(){}
    public PatientEmrClass(String doctor_name, String emr_id, String profession, String hospital) {
        this.doctor_name = doctor_name;
        this.emr_id = emr_id;
        this.profession = profession;
        this.hospital = hospital;
    }

    public String getDoctor_name() {
        return doctor_name;
    }

    public void setDoctor_name(String doctor_name) {
        this.doctor_name = doctor_name;
    }

    public String getEmr_id() {
        return emr_id;
    }

    public void setEmr_id(String emr_id) {
        this.emr_id = emr_id;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }
}
