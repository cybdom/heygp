package com.cybdom.heygp;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.cybdom.heygp.fragments.medications;
import com.cybdom.heygp.fragments.patient_booking;
import com.cybdom.heygp.fragments.patient_calendar;
import com.cybdom.heygp.fragments.patient_health_record;
import com.cybdom.heygp.fragments.patient_main_menu;
import com.cybdom.heygp.fragments.patient_profile;
import com.cybdom.heygp.fragments.pharmacies;
import com.cybdom.heygp.fragments.subscriptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;

public class patient_mainMenu extends AppCompatActivity {
    protected boolean tooglenavigation = false;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_main_menu);
//        Setting up Firebase
        mAuth = FirebaseAuth.getInstance();
        if ( mAuth.getCurrentUser() == null){
            finish();
        }
//      Setting the main menu as current fragment
        patient_main_menu myf = new patient_main_menu();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout_container, myf);
        transaction.commit();
//        Getting input from bottom navigation
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                changePage(item.getItemId());
                return true;
            }
        });
//        Setting up toolbar
        Toolbar toolbar = findViewById(R.id.app_bar);
        this.setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!tooglenavigation) {
                    toogleNavigation(false);
                }
                else {
                    toogleNavigation(true);
                }
            }
        });
//        Side bar buttons
        Button btn_signout = findViewById(R.id.btn_signout);
        ImageButton btn_medication = findViewById(R.id.medication),
                btn_bookings = findViewById(R.id.bookings),
                btn_inbox = findViewById(R.id.inbox),
                btn_health_record= findViewById(R.id.health_record),
                btn_advice = findViewById(R.id.advice),
                btn_insurance = findViewById(R.id.insurance),
                btn_pharmacies = findViewById(R.id.pharmacies),
                btn_settings = findViewById(R.id.settings);

        btn_signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog myDialog;
                myDialog = new Dialog(v.getContext());
                myDialog.setContentView(R.layout.dialog_signout);
                myDialog.show();
                myDialog.findViewById(R.id.btn_true).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mAuth.signOut();
                        patient_mainMenu.super.finish();
                    }
                });
                myDialog.findViewById(R.id.btn_false).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.cancel();
                    }
                });
            }
        });

        btn_medication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                medications myf = new medications();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout_container, myf);
                transaction.commit();
                tooglenavigation = false;
                toogleNavigation(true);
            }
        });
        btn_bookings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                patient_booking myf = new patient_booking();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout_container, myf);
                transaction.commit();
                tooglenavigation = false;
                toogleNavigation(true);
            }
        });
//        btn_inbox.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                medications myf = new medications();
//                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//                transaction.replace(R.id.frame_layout_container, myf);
//                transaction.commit();
//                tooglenavigation = false;
//                toogleNavigation(true);
//            }
//        });
        btn_health_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                patient_health_record myf = new patient_health_record();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout_container, myf);
                transaction.commit();
                tooglenavigation = false;
                toogleNavigation(true);
            }
        });
//        btn_advice.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                medications myf = new medications();
//                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//                transaction.replace(R.id.frame_layout_container, myf);
//                transaction.commit();
//                tooglenavigation = false;
//                toogleNavigation(true);
//            }
//        });
        btn_insurance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subscriptions myf = new subscriptions();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout_container, myf);
                transaction.commit();
                tooglenavigation = false;
                toogleNavigation(true);
            }
        });
        btn_pharmacies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pharmacies myf = new pharmacies();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout_container, myf);
                transaction.commit();
                tooglenavigation = false;
                toogleNavigation(true);
            }
        });
        btn_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), settings.class);
                startActivity(i);
                tooglenavigation = false;
                toogleNavigation(true);
            }
        });
    }
    @Override
    public void onStart(){
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
    }
    public void changePage(int page){
        if ( page == R.id.bottom_nav_home) {
            patient_main_menu myf = new patient_main_menu();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout_container, myf);
            transaction.commit();
        }
        else if ( page == R.id.bottom_nav_booking ) {
            patient_booking myf = new patient_booking();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout_container, myf);
            transaction.commit();
        }
        else if ( page == R.id.bottom_nav_calendar ) {
            patient_calendar myf = new patient_calendar();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout_container, myf);
            transaction.commit();
        }
        else if ( page == R.id.bottom_nav_user ) {
            patient_profile myf = new patient_profile();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout_container, myf);
            transaction.commit();
        }
    }
    public void startMedicineReminder(View view){
        patient_main_menu.startMedicineReminder(view);
    }
    public void startMedicalRecord (View view){
        patient_main_menu.startMedicalRecord(view);
    }
    public void startDietryPlan (View view){
        patient_main_menu.startDietryPlan(view);
    }
    public void startBookDoctor (View view) {
        patient_main_menu.startBookDoctor(view);
    }
    public void startCalendar(View view){
        patient_main_menu.startCalendar(view);
    }
    public void startPatientProfil(View view){
        patient_main_menu.startPatientProfil(view);
    }
    public void startFamily(View view){patient_profile.startFamily(view);}
    public void startSubscriptions(View view){patient_profile.startSubscriptions(view);}
    public void startNotes(View view){patient_profile.startNotes(view);}
    public void startEmergency(View view){patient_profile.startEmergency(view);}
    public void selectCallType(View view){patient_booking.selectCallType(view);}
    public void startPharmacies(View view){patient_main_menu.startPharmacies(view);}
    public void startDoctorPermission(View view){patient_main_menu.startDoctorPermission(view);}
    public void toogleNavigation(boolean out_status){
        if ( !out_status){
            tooglenavigation = true;
            findViewById(R.id.dropdown_layout).setVisibility(View.VISIBLE);
            findViewById(R.id.dropdown_layout).animate().scaleY(1).start();
            findViewById(R.id.frame_layout_container).animate().scaleY(0).start();
        }
        else {
            tooglenavigation = false;
            findViewById(R.id.dropdown_layout).animate().scaleY(0).start();
            findViewById(R.id.frame_layout_container).animate().scaleY(1).start();
            findViewById(R.id.dropdown_layout).setVisibility(View.GONE);
        }
    }
}
