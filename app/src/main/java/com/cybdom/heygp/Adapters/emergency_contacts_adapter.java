package com.cybdom.heygp.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cybdom.heygp.R;
import com.cybdom.heygp.classes.emergency_contacts_class;

import java.util.List;

public class emergency_contacts_adapter extends RecyclerView.Adapter<emergency_contacts_adapter.EmergencyViewHolder> {
    public static class EmergencyViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView full_name, tel, address, email;
        EmergencyViewHolder(View itemView){
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            full_name = itemView.findViewById(R.id.full_name);
            tel = itemView.findViewById(R.id.tel);
            address = itemView.findViewById(R.id.address);
            email = itemView.findViewById(R.id.email);
        }
    }
    List<emergency_contacts_class> mEmergencyContacts;
    public emergency_contacts_adapter(List<emergency_contacts_class> emergency_contacts_classes){
        this.mEmergencyContacts = emergency_contacts_classes;
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
    @NonNull
    @Override
    public EmergencyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_emergency_contact,parent,false);
        EmergencyViewHolder mvh = new EmergencyViewHolder(v);
        return mvh;
    }
    @Override
    public void onBindViewHolder(@NonNull EmergencyViewHolder medecineViewHolder, final int i ) {
        medecineViewHolder.full_name.setText(mEmergencyContacts.get(i).full_name);
    }
    @Override
    public int getItemCount(){
        return mEmergencyContacts.size();
    }
}
