package com.cybdom.heygp.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cybdom.heygp.R;
import com.cybdom.heygp.classes.PatientFamilyClass;

import java.util.List;

public class PatientFamilyAdapter extends RecyclerView.Adapter<PatientFamilyAdapter.PatientFamilyViewHolder> {
    List<PatientFamilyClass> mPatientFamilyClasses;
    public static class PatientFamilyViewHolder extends RecyclerView.ViewHolder{
        CardView cv;
        TextView fullNameTextView,
                other_names,
                ageTextView,
                addressTextView,
                marialStatusTextView,
                phoneNumberTextView,
                emailTextView;
        ImageView dropdown;
        PatientFamilyViewHolder(View itemView){
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            fullNameTextView = itemView.findViewById(R.id.fullNameTextView);
            other_names = itemView.findViewById(R.id.other_names);
            ageTextView = itemView.findViewById(R.id.ageTextView);
            addressTextView = itemView.findViewById(R.id.addressTextView);
            marialStatusTextView = itemView.findViewById(R.id.marialStatusTextView);
            phoneNumberTextView = itemView.findViewById(R.id.phoneNumberTextView);
            emailTextView = itemView.findViewById(R.id.emailTextView);
            dropdown = itemView.findViewById(R.id.dropdown);
        }
    }
    public PatientFamilyAdapter(List<PatientFamilyClass> patientFamilyClasses){
        this.mPatientFamilyClasses = patientFamilyClasses;
    }
    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public PatientFamilyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_patient_family, parent, false);
        return new PatientFamilyViewHolder(v);
    }
    @Override
    public void onBindViewHolder(PatientFamilyViewHolder patientFamilyViewHolder, final int i){
        patientFamilyViewHolder.fullNameTextView.setText(mPatientFamilyClasses.get(i).fullNameString);
        patientFamilyViewHolder.other_names.setText(mPatientFamilyClasses.get(i).other_names);
        patientFamilyViewHolder.ageTextView.setText(mPatientFamilyClasses.get(i).ageString);
        patientFamilyViewHolder.addressTextView.setText(mPatientFamilyClasses.get(i).addressString);
        patientFamilyViewHolder.marialStatusTextView.setText(mPatientFamilyClasses.get(i).marialStatusString);
        patientFamilyViewHolder.phoneNumberTextView.setText(mPatientFamilyClasses.get(i).phoneNumberString);
        patientFamilyViewHolder.emailTextView.setText(mPatientFamilyClasses.get(i).emailString);
        patientFamilyViewHolder.dropdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toogleExpand(view);
            }
        });
    }
    @Override
    public int getItemCount() {
        return mPatientFamilyClasses.size();
    }
    public void toogleExpand(View view) {
        Boolean isVisible = false;
        View expandView = (View)view.getParent().getParent();
        LinearLayout expand = expandView.findViewById(R.id.expanded);
        Log.i("Visibility", String.valueOf(expand.getVisibility()));
        if (expand.getVisibility() == View.VISIBLE) {
            isVisible = true;
        }
        if (expand.getVisibility() == View.GONE) {
            isVisible = false;
        }
        if (isVisible) {
            expand.setVisibility(View.GONE);
            view.animate().rotation(0);
        } else {
            expand.setVisibility(View.VISIBLE);
            view.animate().rotation(180);
        }
    }
}
