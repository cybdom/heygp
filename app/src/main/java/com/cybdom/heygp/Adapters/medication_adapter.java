package com.cybdom.heygp.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cybdom.heygp.R;
import com.cybdom.heygp.classes.medication_class;

import java.util.List;

public class medication_adapter extends RecyclerView.Adapter<medication_adapter.MedicationViewHolder> {
    public static class MedicationViewHolder extends RecyclerView.ViewHolder{
        CardView cv;
        TextView medication_name, medication_dose;
        MedicationViewHolder(View itemView){
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            medication_dose = itemView.findViewById(R.id.medication_dose);
            medication_name = itemView.findViewById(R.id.medication_name);
        }
    }
    List<medication_class> mMedicationClass;
    public medication_adapter(List<medication_class> medicationClass){
        this.mMedicationClass = medicationClass;
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
    @NonNull
    @Override
    public MedicationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_medication,parent,false);
        MedicationViewHolder mvh = new MedicationViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(@NonNull MedicationViewHolder medicationViewHolder, final int i ) {
        medicationViewHolder.medication_name.setText(mMedicationClass.get(i).medication_name);
        medicationViewHolder.medication_dose.setText(mMedicationClass.get(i).medication_dose);
    }
    @Override
    public int getItemCount(){
        return mMedicationClass.size();
    }
}
