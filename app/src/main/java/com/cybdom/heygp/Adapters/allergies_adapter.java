package com.cybdom.heygp.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cybdom.heygp.R;
import com.cybdom.heygp.classes.allergies_class;

import java.util.List;

public class allergies_adapter extends RecyclerView.Adapter<allergies_adapter.allergiesViewHolder> {
    public static class allergiesViewHolder extends RecyclerView.ViewHolder{
        CardView cv;
        TextView allergies;
        allergiesViewHolder(View itemView){
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            allergies = itemView.findViewById(R.id.allergies);
        }
    }
    List<allergies_class> mAllergiesClass;
    public allergies_adapter(List<allergies_class> allergiesClass){
        this.mAllergiesClass = allergiesClass;
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
    @NonNull
    @Override
    public allergies_adapter.allergiesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_allergies,parent,false);
        allergies_adapter.allergiesViewHolder mvh = new allergies_adapter.allergiesViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(@NonNull allergies_adapter.allergiesViewHolder medicationViewHolder, final int i ) {
        medicationViewHolder.allergies.setText(mAllergiesClass.get(i).allergies);

    }
    @Override
    public int getItemCount(){
        return mAllergiesClass.size();
    }
}
