package com.cybdom.heygp.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cybdom.heygp.R;
import com.cybdom.heygp.classes.patient_notification_Class;

import java.util.List;

public class PatientNotificationAdapter extends RecyclerView.Adapter<PatientNotificationAdapter.PatientNotificationViewHolder> {
    List<patient_notification_Class> mPatient_notification_classes;
    public static class PatientNotificationViewHolder extends RecyclerView.ViewHolder{
        CardView cv;
        TextView notification_text;
        ImageView notification_icon;
        PatientNotificationViewHolder (View itemView){
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            notification_text = itemView.findViewById(R.id.notification_text);
            notification_icon = itemView.findViewById(R.id.notification_type_image);
        }
    }
    public PatientNotificationAdapter(List<patient_notification_Class> patient_notification_classes){
        this.mPatient_notification_classes = patient_notification_classes;
    }
    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public PatientNotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_patient_notification, parent, false);
        return new PatientNotificationViewHolder(v);
    }
    @Override
    public void onBindViewHolder(PatientNotificationViewHolder patientNotificationViewHolder, final int i) {
        patientNotificationViewHolder.cv.setCardBackgroundColor(mPatient_notification_classes.get(i).card_color);
        patientNotificationViewHolder.notification_text.setText(mPatient_notification_classes.get(i).notification_text);
        patientNotificationViewHolder.notification_icon.setImageResource(mPatient_notification_classes.get(i).notification_icon);
    }
    @Override
    public int getItemCount() {
        return mPatient_notification_classes.size();
    }
}
