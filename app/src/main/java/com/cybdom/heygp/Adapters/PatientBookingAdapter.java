package com.cybdom.heygp.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cybdom.heygp.R;
import com.cybdom.heygp.classes.PatientBookingClass;

import java.util.List;

public class PatientBookingAdapter extends RecyclerView.Adapter<PatientBookingAdapter.bookingViewHolder> {

    List<PatientBookingClass> mPatientBookingClasses;

    public static class bookingViewHolder extends RecyclerView.ViewHolder{
        CardView cv;
        TextView profession,booking_date;
        public bookingViewHolder(@NonNull View itemView) {
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            profession = itemView.findViewById(R.id.profession);
            booking_date = itemView.findViewById(R.id.booking_date);
        }
    }
    public PatientBookingAdapter(List<PatientBookingClass> patientBookingClasses){
        this.mPatientBookingClasses = patientBookingClasses;
    }
    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView){
        super.onAttachedToRecyclerView(recyclerView);
    }
    @NonNull
    @Override
    public PatientBookingAdapter.bookingViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_patient_booking,viewGroup,false);
        return new bookingViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PatientBookingAdapter.bookingViewHolder bookingViewHolder, int i) {
        bookingViewHolder.booking_date.setText(mPatientBookingClasses.get(i).getDate());
        bookingViewHolder.profession.setText(mPatientBookingClasses.get(i).getProfession());
    }

    @Override
    public int getItemCount() {
        return mPatientBookingClasses.size();
    }
}
