package com.cybdom.heygp.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cybdom.heygp.R;
import com.cybdom.heygp.classes.reminderClass;

import java.util.List;

public class Medecine_reminder_adapter extends RecyclerView.Adapter<Medecine_reminder_adapter.MedecineViewHolder> {
    public static class MedecineViewHolder extends RecyclerView.ViewHolder{
        CardView cv;
        TextView medication_TextView, schedule_TextView, notes_TextView, schedule2_TextView;
        ImageView dropdown;
        MedecineViewHolder(View itemView){
            super(itemView);
            cv = itemView.findViewById(R.id.card_medecine_reminder);
            medication_TextView = itemView.findViewById(R.id.medication);
            schedule_TextView = itemView.findViewById(R.id.schedule);
            notes_TextView = itemView.findViewById(R.id.notes);
            schedule2_TextView = itemView.findViewById(R.id.schedule2);
            dropdown = itemView.findViewById(R.id.dropdown);
        }
    }
    List<reminderClass> mReminderClasses;
    public Medecine_reminder_adapter(List<reminderClass> reminderClasses){
        this.mReminderClasses = reminderClasses;
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
    @NonNull
    @Override
    public MedecineViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_medecine_reminder,parent,false);
        MedecineViewHolder mvh = new MedecineViewHolder(v);
        return mvh;
    }
    @Override
    public void onBindViewHolder(@NonNull MedecineViewHolder medecineViewHolder, final int i ){
        medecineViewHolder.medication_TextView.setText(mReminderClasses.get(i).medication_string);
        medecineViewHolder.notes_TextView.setText(mReminderClasses.get(i).notes_string);
        medecineViewHolder.schedule_TextView.setText(mReminderClasses.get(i).schedule_string);
        medecineViewHolder.schedule2_TextView.setText(mReminderClasses.get(i).schedule_string);
        medecineViewHolder.dropdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toogleExpand(view);
            }
        });
    }
    @Override
    public int getItemCount(){
        return mReminderClasses.size();
    }
    public void toogleExpand(View view) {
        Boolean isVisible = false;
        View expandView = (View)view.getParent().getParent();
        LinearLayout expand = expandView.findViewById(R.id.expanded);
        Log.i("Visibility", String.valueOf(expand.getVisibility()));
        if (expand.getVisibility() == View.VISIBLE) {
            isVisible = true;
        }
        if (expand.getVisibility() == View.GONE) {
            isVisible = false;
        }
        if (isVisible) {
            expand.setVisibility(View.GONE);
            view.animate().rotation(0);
        } else {
            expand.setVisibility(View.VISIBLE);
            view.animate().rotation(180);
        }
    }
}
