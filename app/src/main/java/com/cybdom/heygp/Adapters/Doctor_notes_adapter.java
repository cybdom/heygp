package com.cybdom.heygp.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cybdom.heygp.R;
import com.cybdom.heygp.classes.Doctor_notes_class;

import java.util.List;

public class Doctor_notes_adapter extends RecyclerView.Adapter<Doctor_notes_adapter.NotesViewHolder> {
    public static class NotesViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView name_surname, date, location, note;

        NotesViewHolder(View itemView) {
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            name_surname = itemView.findViewById(R.id.name_surname);
            date = itemView.findViewById(R.id.date);
            location = itemView.findViewById(R.id.location);
            note = itemView.findViewById(R.id.note);
        }
    }
    List<Doctor_notes_class> mDoctor_notes_class;
    public Doctor_notes_adapter(List<Doctor_notes_class> doctor_notes_class){
        this.mDoctor_notes_class = doctor_notes_class;
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
    @NonNull
    @Override
    public NotesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_doctor_note,parent,false);
        NotesViewHolder mvh = new NotesViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(@NonNull NotesViewHolder medecineViewHolder, final int i ) {
        medecineViewHolder.name_surname.setText(mDoctor_notes_class.get(i).name_surname);
        medecineViewHolder.date.setText(mDoctor_notes_class.get(i).date);
        medecineViewHolder.location.setText(mDoctor_notes_class.get(i).location);
        medecineViewHolder.note.setText(mDoctor_notes_class.get(i).note);
    }
    @Override
    public int getItemCount(){
        return mDoctor_notes_class.size();
    }
}
