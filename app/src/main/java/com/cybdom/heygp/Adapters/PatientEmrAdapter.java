package com.cybdom.heygp.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cybdom.heygp.R;
import com.cybdom.heygp.classes.PatientEmrClass;

import java.util.List;

public class PatientEmrAdapter extends RecyclerView.Adapter<PatientEmrAdapter.PatientEmrViewHolder> {
    List<PatientEmrClass> mPatientEmrClasses;


    public static class PatientEmrViewHolder extends RecyclerView.ViewHolder{
        CardView cv;
        TextView doctor_name, emr_id, profession, hospital;
        ImageView dropdown;
        PatientEmrViewHolder(View itemView){
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            doctor_name = itemView.findViewById(R.id.doctor_name);
            emr_id = itemView.findViewById(R.id.emr_id);
            profession = itemView.findViewById(R.id.profession);
            hospital = itemView.findViewById(R.id.hospital);
            dropdown = itemView.findViewById(R.id.dropdown);
        }
    }
    public PatientEmrAdapter(List<PatientEmrClass> patientEmrClasses){
        this.mPatientEmrClasses = patientEmrClasses;
    }
    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView){
        super.onAttachedToRecyclerView(recyclerView);
    }
    @NonNull
    @Override
    public PatientEmrViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_patient_emr,parent,false);
        return new PatientEmrViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PatientEmrViewHolder holder, int position) {
        holder.doctor_name.setText(mPatientEmrClasses.get(position).doctor_name);
        holder.emr_id.setText(mPatientEmrClasses.get(position).emr_id);
        holder.profession.setText(mPatientEmrClasses.get(position).profession);
        holder.hospital.setText(mPatientEmrClasses.get(position).hospital);
        holder.dropdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toogleExpand(view);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPatientEmrClasses.size();
    }
    public void toogleExpand(View view) {
        Boolean isVisible = false;
        View expandView = (View)view.getParent().getParent();
        LinearLayout expand = expandView.findViewById(R.id.expanded);
        Log.i("Visibility", String.valueOf(expand.getVisibility()));
        if (expand.getVisibility() == View.VISIBLE) {
            isVisible = true;
        }
        if (expand.getVisibility() == View.GONE) {
            isVisible = false;
        }
        if (isVisible) {
            expand.setVisibility(View.GONE);
            view.animate().rotation(0);
        } else {
            expand.setVisibility(View.VISIBLE);
            view.animate().rotation(180);
        }
    }
}
