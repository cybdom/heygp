package com.cybdom.heygp.Adapters;

import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.cybdom.heygp.R;
import com.cybdom.heygp.classes.pharmacie_class;
import com.cybdom.heygp.fragments.pharmacies;

import java.util.List;

public class pharmacie_adapter extends RecyclerView.Adapter<pharmacie_adapter.pharmacieViewHolder> {
    private Dialog myDialog;
    public static class pharmacieViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView pharmacy_name,pharmacie_address, pharmacie_hours;
        ImageButton pharmacy_action;
        pharmacieViewHolder(View itemView) {
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            pharmacy_name = itemView.findViewById(R.id.pharmacy_name);
            pharmacie_address = itemView.findViewById(R.id.pharmacie_address);
            pharmacie_hours = itemView.findViewById(R.id.pharmacie_hours);
            pharmacy_action = itemView.findViewById(R.id.pharmacy_action);
        }
    }

    List<pharmacie_class> mPharmacieClass;

    public pharmacie_adapter(List<pharmacie_class> pharmacieClass ) {
        this.mPharmacieClass = pharmacieClass;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public pharmacie_adapter.pharmacieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_pharmacie, parent, false);
        pharmacie_adapter.pharmacieViewHolder mvh = new pharmacie_adapter.pharmacieViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(@NonNull pharmacie_adapter.pharmacieViewHolder PharmacieViewHolder, final int i) {
        PharmacieViewHolder.pharmacy_name.setText(mPharmacieClass.get(i).pharmacy_name);
        PharmacieViewHolder.pharmacie_address.setText(mPharmacieClass.get(i).pharmacie_address);
        PharmacieViewHolder.pharmacie_hours.setText(mPharmacieClass.get(i).pharmacie_hours);
        PharmacieViewHolder.pharmacy_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pharmacies.openDialog(v);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPharmacieClass.size();
    }
}
