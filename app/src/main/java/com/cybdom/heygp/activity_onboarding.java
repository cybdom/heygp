package com.cybdom.heygp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.cybdom.heygp.onboarding.onboarding_fragment_1;
import com.cybdom.heygp.onboarding.onboarding_fragment_2;
import com.cybdom.heygp.onboarding.onboarding_fragment_3;
import com.cybdom.heygp.onboarding.onboarding_fragment_end;

public class activity_onboarding extends AppCompatActivity {

    int index = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);
        Button next = findViewById(R.id.next);
        FrameLayout frameLayout = findViewById(R.id.frame_layout_container);

        final onboarding_fragment_1 fragment1 = new onboarding_fragment_1();
        final onboarding_fragment_2 fragment2 = new onboarding_fragment_2();
        final onboarding_fragment_3 fragment3 = new onboarding_fragment_3();
        final onboarding_fragment_end fragment_end = new onboarding_fragment_end();


        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout_container, fragment1);
        transaction.commit();

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( index == 1 ){
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame_layout_container, fragment2);
                    transaction.commit();
                }
                else if ( index == 2 ){
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame_layout_container, fragment3);
                    transaction.commit();
                }
                else if ( index == 3 ){
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame_layout_container, fragment_end);
                    transaction.commit();
                }
                else if ( index == 4 ){
                    Intent i = new Intent(getApplicationContext(), patient_login.class);
                    startActivity(i);
                    index = 0;
                }
                index++;
            }
        });
    }
}
