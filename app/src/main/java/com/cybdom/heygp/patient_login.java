package com.cybdom.heygp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class patient_login extends AppCompatActivity {
    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_login);
        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null){
            Intent i = new Intent(getApplicationContext(), patient_mainMenu.class);
            startActivity(i);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAuth = FirebaseAuth.getInstance();
        updateUI(mAuth.getCurrentUser());
    }

    public void facebookLogin(View view){
        Intent i = new Intent(getApplicationContext(), patient_mainMenu.class);
        startActivity(i);
    }
    public void googleLogin(View view){
        Intent i = new Intent(getApplicationContext(), patient_mainMenu.class);
        startActivity(i);
    }
    public void emailLogin(View view){
        String email, password;

        email = ((EditText)findViewById(R.id.email)).getText().toString();;
        password = ((EditText)findViewById(R.id.password)).getText().toString();
        if (inputValidation(email,password)) {
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d("Signed In", "signInWithEmail:success");
                                FirebaseUser user = mAuth.getCurrentUser();
                                updateUI(user);
                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w("Failed to sign In", "signInWithEmail:failure", task.getException());
                                Toast.makeText(getApplicationContext(), "Authentication failed.",
                                        Toast.LENGTH_SHORT).show();
                                updateUI(null);
                            }
                        }
                    });
        }
    }
    public void signUp(View view){
        Intent i = new Intent(getApplicationContext(), activity_signup.class);
        startActivity(i);
    }
    public void updateUI(FirebaseUser user){
        if ( user != null ){
            Intent i = new Intent(getApplicationContext(), patient_mainMenu.class);
            startActivity(i);
        }
    }
    public Boolean inputValidation(String email, String password){
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            Toast.makeText(this, "Invalid Email Address", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (password.length() < 1){
            Toast.makeText(this, "Enter your password", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (password.length() < 8){
            Toast.makeText(this, "Invalid password", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
